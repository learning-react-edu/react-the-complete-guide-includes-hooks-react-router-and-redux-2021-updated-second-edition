let myLetName = 'Edu'; // this declares mutable variable
console.log(myLetName);
myLetName = 'Finn';
console.log(myLetName);

const myConstName = 'Edu'; // this declares immutable variable or constant
console.log(myConstName);
// next line will fail, because constant cannot be changed
// myConstName = 'Finn';
