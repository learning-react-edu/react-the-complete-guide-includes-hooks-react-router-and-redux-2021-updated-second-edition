function printMyName(name) {
    console.log(name);
}
printMyName('Edu');

const printMyNameAgain = name => {
    console.log(name);
}
printMyNameAgain('Finn');

const printSomething = () => {
    console.log('something');
}
printSomething();

const printNameAndAge = (name, age) => {
    console.log(name, age);
}
printNameAndAge('Edu', 53);

const multiply = number => number * 2;
console.log(multiply(2));
