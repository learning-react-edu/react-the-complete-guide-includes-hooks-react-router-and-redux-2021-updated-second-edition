class Human {
    constructor() {
        this.gender = 'male';
    }

    printGender() {
        console.log(this.gender);
    }
}

class Person extends Human {
    constructor() {
        super();
        this.name = 'Janna';
        this.gender = 'female';
    }

    printMyName() {
        console.log(this.name);
    }
}

const person = new Person();
person.printMyName();
person.printGender();

class HumanES {
    gender = 'female';

    printGender = () => {
        console.log(this.gender);
    }
}

class PersonES extends HumanES {
    name = 'Edu';
    gender = 'male';

    printMyName = () => {
        console.log(this.name);
    }
}

const personEs = new PersonES();
personEs.printMyName();
personEs.printGender();
