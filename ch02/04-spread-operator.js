// spread operator
const numbers = [1, 2, 3];
const newNumbers = [...numbers, 4, 5];
console.log(newNumbers);

const person = {
    name: 'Edu'
};

const newPerson = {
    ...person,
    age: 53
};
console.log(newPerson);

// rest operator
const filter = (...args) => args.filter(el => el  === 5);
console.log(filter(4, 5, 6));

// destructuring
[num1, , num2] = numbers;
console.log(num1, num2);

const { name } = newPerson;
console.log(newPerson);
console.log(name);
