// primitive types
let number = 5; // number
const num2 = number; // this copy the value
number = 6;
console.log(number);
console.log(num2);

// reference types
const person = {
    name: 'Edu'
};
const secondPerson = person; // this will copy the reference (pointer to memory location)
console.log(secondPerson);
person.name = 'Janna';
console.log(secondPerson);

const personCopy = {
    ...person
};
console.log(personCopy);
person.name = 'Arno';
console.log(personCopy);
