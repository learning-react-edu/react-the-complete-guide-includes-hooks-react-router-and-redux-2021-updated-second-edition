import './Expenses.css';
import { ExpenseItem } from "./ExpenseItem";
import { Card } from "../UI/Card";

export const Expenses = ({ expenses }) => (
    <Card className="expenses">
        {expenses.map(expense => (
            <ExpenseItem
                key={expense.id}
                expenseAmount={expense.amount}
                expenseDate={expense.date}
                expenseTitle={expense.title}
            />
        ))}
    </Card>
);
