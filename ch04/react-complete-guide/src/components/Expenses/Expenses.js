import { useState } from 'react';
import './Expenses.css';
import { ExpenseItem } from './ExpenseItem';
import { Card } from '../UI/Card';
import { ExpensesFilter } from './ExpensesFilter';

export const Expenses = ({ expenses }) => {
  const [filteredYear, setFilteredYear] = useState('2020');

  const filterChangeHandler = selectedYear => {
    setFilteredYear(selectedYear);
  };

  return (
    <Card className="expenses">
      <ExpensesFilter selected={filteredYear} onChangeFilter={filterChangeHandler}/>
      {expenses.map(expense => (
        <ExpenseItem
          key={expense.id}
          expenseAmount={expense.amount}
          expenseDate={expense.date}
          expenseTitle={expense.title}
        />
      ))}
    </Card>
  );
};
