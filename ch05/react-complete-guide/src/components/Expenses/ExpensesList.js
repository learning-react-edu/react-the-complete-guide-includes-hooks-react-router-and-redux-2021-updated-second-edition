import './ExpensesList.css';
import { ExpenseItem } from './ExpenseItem';

const ExpensesList = ({ expenses }) => expenses.length === 0 ? (
  <h2 className='expenses-list__fallback'>Found no expenses.</h2>
) : (
  <ul className='expenses-list'>
    {expenses.map(expense => (
      <ExpenseItem
        key={expense.id}
        expenseTitle={expense.title}
        expenseAmount={expense.amount}
        expenseDate={expense.date}
      />
    ))}
  </ul>
);


export {
  ExpensesList,
};
