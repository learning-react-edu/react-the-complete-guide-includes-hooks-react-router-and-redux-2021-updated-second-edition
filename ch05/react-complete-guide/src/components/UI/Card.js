import './Card.css';

export const Card = ({ className, children }) => (
  <div className={`card ${className}`}>
    {children}
  </div>
);
