import { useContext } from 'react';
import { Modal } from '../UI/Modal';
import { CartContext } from '../../store/CartContext';
import classes from './Cart.module.css';
import { CartItem } from './CartItem';

const Cart = ({ onClose }) => {
  const cartContext = useContext(CartContext);
  const totalAmount = `$${cartContext.totalAmount.toFixed(2)}`;
  const hasItems = cartContext.items.length > 0;

  const cartItemRemoveHandler = id => {
    cartContext.removeItem(id);
  };

  const cartItemAddHandler = item => {
    cartContext.addItem({ ...item, amount: 1 })
  };

  return (
    <Modal onClose={onClose}>
      <ul className={classes['cart-items']}>
        {cartContext.items.map(item => (
          <CartItem
            key={item.id}
            name={item.name}
            amount={item.amount}
            price={item.price}
            onAdd={cartItemAddHandler.bind(null, item)}
            onRemove={cartItemRemoveHandler.bind(null, item.id)}
          />
        ))}
      </ul>
      <div className={classes.total}>
        <span>Total Amount</span>
        <span>{totalAmount}</span>
      </div>
      <div className={classes.actions}>
        <button className={classes['button--alt']} onClick={onClose}>Close</button>
        { hasItems && <button className={classes.button}>Order</button> }
      </div>
    </Modal>
  );
};

export {
  Cart,
};
