import { CartContext } from './CartContext';
import { useReducer } from 'react';

const defaultCartState = {
  items: [],
  totalAmount: 0,
};

const cartReducer = (state, action) => {
  if (action.type === 'ADD') {
    const updatedTotalAmount = state.totalAmount + action.payload.price * action.payload.amount;
    const existingCartItemIndex = state.items.findIndex(item => item.id === action.payload.id);
    const existingCartItem = state.items[existingCartItemIndex];

    const updatedItems = existingCartItemIndex < 0 ?
      state.items.concat(action.payload) :
      [ ...state.items.slice(0, existingCartItemIndex),
        { ...existingCartItem, amount: existingCartItem.amount + action.payload.amount },
        ...state.items.slice(existingCartItemIndex + 1) ];

    return {
      items: updatedItems,
      totalAmount: updatedTotalAmount,
    };
  }
  if (action.type === 'REMOVE') {
    const existingCartItemIndex = state.items.findIndex(item => item.id === action.payload);
    const existingItem = state.items[existingCartItemIndex];
    const updatedTotalAmount = state.totalAmount - existingItem.price;
    const updatedItems = existingItem.amount === 1 ?
      state.items.filter(item => item.id !== action.payload) :
      [ ...state.items.slice(0, existingCartItemIndex),
        { ...existingItem, amount: existingItem.amount - 1 },
        ...state.items.slice(existingCartItemIndex + 1) ];
    return {
      items: updatedItems,
      totalAmount: updatedTotalAmount,
    };
  }
  return defaultCartState;
};

const CartProvider = ({ children }) => {
  const [ cartState, dispatchCartAction ] = useReducer(cartReducer, defaultCartState);

  const addItemToCartHandler = item => {
    dispatchCartAction({
      type: 'ADD',
      payload: item,
    });
  };

  const removeItemFromCartHandler = id => {
    dispatchCartAction({
      type: 'REMOVE',
      payload: id,
    });
  };

  const cartContext = {
    items: cartState.items,
    totalAmount: cartState.totalAmount,
    addItem: addItemToCartHandler,
    removeItem: removeItemFromCartHandler,
  };

  return (
    <CartContext.Provider value={cartContext}>
      {children}
    </CartContext.Provider>
  );
};

export {
  CartProvider,
};
