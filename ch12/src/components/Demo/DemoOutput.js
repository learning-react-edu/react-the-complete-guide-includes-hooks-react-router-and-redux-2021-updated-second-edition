import React from 'react';
import { MyParagraph } from './MyParagraph';

const DemoOutput = React.memo(({ show }) => {
  console.log('DemoOutput RUNNING');

  return (
    <MyParagraph>{show ? 'This is new!' : ''}</MyParagraph>
  );
});

export {
  DemoOutput,
};
