import React from 'react';

import classes from './Button.module.css';

const Button = React.memo(({ children, className, disabled, onClick, type }) => {
  console.log('Button RUNNING');

  return (
    <button
      type={type || 'button'}
      className={`${classes.button} ${className}`}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
});

export {
  Button,
};
