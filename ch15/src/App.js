import React, { useEffect, useState } from 'react';
// import BackwardCounter from './components/Counters/BackwardCounter';
// import ForwardCounter from './components/Counters/ForwardCounter';
import NewTask from './components/NewTask/NewTask';
import Tasks from './components/Tasks/Tasks';
import { useHttp } from './hooks/use-http';

export const URL = 'https://react-http-edu-default-rtdb.europe-west1.firebasedatabase.app/tasks.json';

function App() {
  const [tasks, setTasks] = useState([]);

  const { isLoading, error, sendRequest: fetchTasks } = useHttp();

  useEffect(() => {
    const transformTasks = tasksObj => {
      const loadedTasks = Object
      .entries(tasksObj)
      .map(([key, value]) => ({ id: key, text: value.text }));
      setTasks(loadedTasks);
    };

    fetchTasks({url: URL}, transformTasks);
  }, [fetchTasks]);

  const taskAddHandler = (task) => {
    setTasks((prevTasks) => prevTasks.concat(task));
  };

  return (
    <React.Fragment>
{/*
      <ForwardCounter />
      <BackwardCounter />
*/}
      <NewTask onAddTask={taskAddHandler} />
      <Tasks
        items={tasks}
        loading={isLoading}
        error={error}
        onFetch={fetchTasks}
      />
    </React.Fragment>
  );
}

export default App;
