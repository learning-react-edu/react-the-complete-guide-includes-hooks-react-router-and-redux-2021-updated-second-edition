import { useEffect, useState } from 'react';

const useCounter = (forwards = true, defaultValue = 0) => {
  const [counter, setCounter] = useState(defaultValue);

  useEffect(() => {
    const interval = setInterval(() => {
      if (forwards) {
        setCounter((prevCounter) => prevCounter + 1);
      } else {
        setCounter((prevCounter) => prevCounter - 1);
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [forwards]);

  return counter;
};

export {
  useCounter,
};
