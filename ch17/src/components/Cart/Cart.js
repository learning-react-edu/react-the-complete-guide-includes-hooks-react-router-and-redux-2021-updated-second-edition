import { useContext, useState } from 'react';
import { Modal } from '../UI/Modal';
import { CartContext } from '../../store/CartContext';
import classes from './Cart.module.css';
import { CartItem } from './CartItem';
import { Checkout } from './Checkout';

const Cart = ({ onClose }) => {
  const [ isCheckout, setIsCheckout ] = useState(false);
  const [ isSubmitting, setIsSubmitting ] = useState(false);
  const [ didSubmit, setDidSubmit ] = useState(false);
  const cartContext = useContext(CartContext);
  const totalAmount = `$${cartContext.totalAmount.toFixed(2)}`;
  const hasItems = cartContext.items.length > 0;

  const cartItemRemoveHandler = id => {
    cartContext.removeItem(id);
  };

  const cartItemAddHandler = item => {
    cartContext.addItem({ ...item, amount: 1 })
  };

  const orderHandler = () => {
    setIsCheckout(true);
  };

  const submitOrderHandler = async userData => {
    setIsSubmitting(true);
    await fetch('https://react-http-edu-default-rtdb.europe-west1.firebasedatabase.app/orders.json', {
      method: 'POST',
      body: JSON.stringify({
        user: userData,
        orderedItems: cartContext.items,
      }),
    });
    setIsSubmitting(false);
    setDidSubmit(true);
    cartContext.clearCart();
  };

  return (
    <Modal onClose={onClose}>
      { !isSubmitting && !didSubmit && <>
        <ul className={classes['cart-items']}>
          {cartContext.items.map(item => (
            <CartItem
              key={item.id}
              name={item.name}
              amount={item.amount}
              price={item.price}
              onAdd={cartItemAddHandler.bind(null, item)}
              onRemove={cartItemRemoveHandler.bind(null, item.id)}
            />
          ))}
        </ul>
        <div className={classes.total}>
          <span>Total Amount</span>
          <span>{totalAmount}</span>
        </div>
        { isCheckout && <Checkout onConfirm={submitOrderHandler} onCancel={onClose}/> }
        { !isCheckout &&
          <div className={classes.actions}>
            <button className={classes['button--alt']} onClick={onClose}>Close</button>
            { hasItems && <button className={classes.button} onClick={orderHandler}>Order</button> }
          </div>
        }
      </>}
      { isSubmitting && <p>Sending order data...</p>}
      { !isSubmitting && didSubmit &&
        <>
          <p>Successfully sent the order!</p>
          <div className={classes.actions}>
            <button className={classes.button} onClick={onClose}>Close</button>
          </div>
        </>}
    </Modal>
  );
};

export {
  Cart,
};
