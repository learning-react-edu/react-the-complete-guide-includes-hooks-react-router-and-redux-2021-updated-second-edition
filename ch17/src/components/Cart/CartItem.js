import classes from './CartItem.module.css';

const CartItem = ({ name, price, amount, onRemove, onAdd }) => (
  <li className={classes['cart-item']}>
    <div>
      <h2>{name}</h2>
      <div className={classes.summary}>
        <span className={price}>{`$${price.toFixed(2)}`}</span>
        <span className={amount}>x {amount}</span>
      </div>
    </div>
    <div className={classes.actions}>
      <button onClick={onRemove}>−</button>
      <button onClick={onAdd}>+</button>
    </div>
  </li>
);

export {
  CartItem,
};
