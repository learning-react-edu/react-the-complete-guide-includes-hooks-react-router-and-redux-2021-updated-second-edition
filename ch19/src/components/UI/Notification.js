import classes from './Notification.module.css';

const Notification = ({ message, status, title }) => {
  const specialClasses =
    status === 'error' ?
      classes.error :
      status === 'success' ?
        classes.success :
        '';

  return (
    <section className={`${classes.notification} ${specialClasses}`}>
      <h2>{title}</h2>
      <p>{message}</p>
    </section>
  );
};

export {
  Notification,
};
