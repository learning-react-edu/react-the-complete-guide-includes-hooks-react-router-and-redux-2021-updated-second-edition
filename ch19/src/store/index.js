import { configureStore } from '@reduxjs/toolkit';
import { uiActions, uiReducer } from './uiSlice';
import { cartReducer, cartActions } from './cartSlice';
import { fetchCartData, sendCartData } from './cartActions';

const store = configureStore({
  reducer: {
    ui: uiReducer,
    cart: cartReducer,
  },
});

export {
  store,
  cartActions,
  uiActions,
  fetchCartData,
  sendCartData,
}
