import { useCallback, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import classes from './Comments.module.css';
import NewCommentForm from './NewCommentForm';
import { useHttp } from '../../hooks/use-http';
import { getAllComments } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import CommentsList from './CommentsList';

const Comments = () => {
  const [ isAddingComment, setIsAddingComment ] = useState(false);
  const { quoteId } = useParams();
  const { sendRequest, status, data: loadedComments } = useHttp(getAllComments);

  useEffect(() => {
    sendRequest(quoteId);
  }, [ sendRequest, quoteId ]);

  const startAddCommentHandler = () => {
    setIsAddingComment(true);
  };

  const addedCommentHandler = useCallback(() => {
    sendRequest(quoteId);
    setIsAddingComment(false);
  }, [sendRequest, quoteId]);

  return (
    <section className={classes.comments}>
      <h2>User Comments</h2>
      {!isAddingComment && (
        <button className='btn' onClick={startAddCommentHandler}>
          Add a Comment
        </button>
      )}
      {isAddingComment && <NewCommentForm quoteId={quoteId} onAddedComment={addedCommentHandler}/>}
      {status === 'pending' && (
        <div className='centered'>
          <LoadingSpinner/>
        </div>
      )}
      {status === 'completed' && loadedComments && loadedComments.length > 0 &&
        <CommentsList comments={loadedComments}/>}
      {status === 'completed' && (!loadedComments || loadedComments.length === 0) &&
        <p className='centered'>No comments where added yet!</p>}
    </section>
  );
};

export default Comments;
