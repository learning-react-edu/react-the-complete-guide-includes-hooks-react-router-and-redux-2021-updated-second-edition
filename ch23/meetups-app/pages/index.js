import Head from 'next/head';
import { MongoClient } from 'mongodb';
import MeetupList from '../components/meetups/MeetupList';

const HomePage = ({ meetups }) => (
  <>
    <Head>
      <title>Add a New Meetups</title>
      <meta
        name='description'
        content='Add your own meetups.'
      />
    </Head>
    <MeetupList meetups={meetups}/>
  </>
);

// export async function getServerSideProps(context) {
//   const req = context.req;
//   const res = context.res;
//
//    // fetch data from an API
//   return {
//     props: {
//       meetups: DUMMY_MEETUPS,
//     },
//   };
// }

export async function getStaticProps() {
  // fetch data from an API
  const client = await MongoClient.connect('mongodb+srv://edu:edu123@cluster0.yzvwhwo.mongodb.net/meetups?retryWrites=true&w=majority');
  const db = client.db();
  const meetupsCollection = db.collection('meetups');

  const meetups = await meetupsCollection.find().toArray();
  client.close();

  return {
    props: {
      meetups: meetups.map(meetup => ({
        title: meetup.title,
        address: meetup.address,
        image: meetup.image,
        id: meetup._id.toString(),
      })),
    },
    revalidate: 1,
  };
}

export default HomePage;
