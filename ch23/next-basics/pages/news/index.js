import Link from 'next/link';

const NewsPage = () => (
  <>
    <h1>The News Page</h1>
    <ul>
      <li><Link href='/news/nextjs-article'>NextJS is a nice framework</Link></li>
      <li><Link href='/news/something-else'>Something else</Link></li>
    </ul>
  </>
);

export default NewsPage;
