import React, { useState } from 'react';
import { Transition } from 'react-transition-group';

import Modal from './components/Modal/Modal';
import Backdrop from './components/Backdrop/Backdrop';
import List from './components/List/List';
import './App.css';

function App() {
  const [ modalIsOpen, setModalIsOpen ] = useState(false);
  const [ showBlock, setShowBlock ] = useState(false);

  const showModal = () => setModalIsOpen(true);

  const closeModal = () => setModalIsOpen(false);

  return (
    <div className='App'>
      <h1>React Animations</h1>
      <button
        className='Button'
        onClick={() => setShowBlock(prevState => !prevState)}
      >
        Toggle
      </button>
      <br/>
      <Transition
        in={showBlock}
        timeout={300}
        mountOnEnter
        unmountOnExit
        onEnter={() => console.log('onEnter')}
        onEntering={() => console.log('onEntering')}
        onEntered={() => console.log('onEntered')}
        onExit={() => console.log('onExit')}
        onExiting={() => console.log('onExiting')}
        onExited={() => console.log('onExited')}
      >
        {state => (<div style={{
          backgroundColor: 'red',
          width: '100px',
          height: '100px',
          margin: 'auto',
          transition: 'opacity 1s ease-out',
          opacity: state === 'exiting' ? 0 : 1,
        }}>
        </div>)}
      </Transition>
      <Modal show={modalIsOpen} closed={closeModal}/>
      {modalIsOpen ? <Backdrop show={modalIsOpen}/> : null}
      <button className='Button' onClick={showModal}>Open Modal</button>
      <h3>Animating Lists</h3>
      <List/>
    </div>
  );
}

export default App;
