import React from 'react';
import { Outlet, Route, Routes } from 'react-router-dom';

import Navigation from './components/Nav/Navigation';
import ProductsPage from './containers/Products';
import FavoritesPage from './containers/Favorites';

const App = () => {
  return (
    <React.Fragment>
      <Navigation />
      <main>
        <Routes>
          <Route path="/" element={<ProductsPage />}  />
          <Route path="/favorites" element={<FavoritesPage />} />
        </Routes>
        <Outlet />
      </main>
    </React.Fragment>
  );
};

export default App;
