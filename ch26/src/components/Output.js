const Output = ({ children }) => (
  <p>{children}</p>
);

export {
  Output,
};
