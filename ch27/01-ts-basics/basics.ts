// Primitives: number, string, boolean
// More complex types: arrays, objects
// Function types, parameters

// Primitives

let age: number;

age = 12;

let userName: string;

userName = 'Max';

let isInstructor: boolean;

isInstructor = true;

// More complex types

let hobbies: string[];

hobbies = ['Sports', 'Cooking'];

let person: {
  name: string;
  age: number;
};

person = {
  name: 'Max',
  age: 32
};

// person = {
//   isEmployee: true
// };

let people: {
  name: string;
  age: number;
}[];

// Type inference

let course = 'React - The Complete Guide';
// next line will fail
// course = 12341;

// Union types

let course2: string | number = 'React - The Complete Guide';
course2 = 12341;

// type aliases
type Person = {
  name: string;
  age: number;
};
let person2: Person;
person2 = {
  name: 'Edu',
  age: 54,
}
let people2: Person[];

// Functions & types

function add(a: number, b: number) {
  return a + b;
}

const addArrow = (a: number, b: number) => a + b;

function print(value: any) {
  console.log(value);
}

print(add(2, 5));
print(addArrow(2, 5));

// Generics

function insertAtBeginning<T>(array: T[], value: T) {
  return [value, ...array];
}

const demoArray = [1, 2, 3];
const updatedArray = insertAtBeginning(demoArray, -1); // [-1, 1, 2, 3]
const stringArray = insertAtBeginning(['a', 'b', 'c'], 'd')

// without type definition line below will compile but will fail during runtime
// with generics next line will not compile
// updatedArray[0].split(''); // split is a string function and cannot be applied to numbers

// this line will compile, because TypeScript recognise it as an array of strings
stringArray[0].split('');
