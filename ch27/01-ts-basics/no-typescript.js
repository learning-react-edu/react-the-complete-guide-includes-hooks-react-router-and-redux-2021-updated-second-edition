function add(a, b) {
  return a + b;
}

const result = add(2, 5);
console.log(result);

const result2 = add('2', '5');
console.log(result2);
