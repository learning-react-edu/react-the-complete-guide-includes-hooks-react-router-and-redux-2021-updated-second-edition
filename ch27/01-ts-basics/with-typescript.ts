function add(a: number, b: number) {
  return a + b;
}

// next line will not compile because of type mismatch
//const result2 = add('2', '5');
const result = add(2, 5);
console.log(result);
