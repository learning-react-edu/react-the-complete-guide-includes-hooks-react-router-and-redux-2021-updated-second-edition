import React from "react";
import {Todo} from "../models/todo";
import classes from './TodoItem.module.css';

const TodoItem: React.FC<{ todo: Todo, onRemoveTodo: () => void }> = ({ todo, onRemoveTodo }) => {
  return <li className={classes.item} onClick={onRemoveTodo}>{todo.text}</li>;
}

export {
  TodoItem,
}
