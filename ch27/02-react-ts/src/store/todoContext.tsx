import React, {useState} from 'react';
import {Todo} from '../models/todo';

type TodoContext = {
  items: Todo[],
  addTodo: (text: string) => void,
  removeTodo: (id: string) => void,
}

const TodosContext = React.createContext<TodoContext>({
  items: [],
  addTodo: () => {},
  removeTodo: () => {},
});

const TodosContextProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [todos, setTodos] = useState<Todo[]>([]);

  const addTodoHandler = (text: string) => {
    const newTodo = new Todo(text);
    setTodos(prevState => [...prevState, newTodo]);
  };

  const removeTodoHandler = (todoId: string) => {
    setTodos(prevState => prevState.filter(todo => todo.id !== todoId));
  };

  const contextValue: TodoContext = {
    items: todos,
    addTodo: addTodoHandler,
    removeTodo: removeTodoHandler,
  };

  return (
    <TodosContext.Provider value={contextValue}>
      {children}
    </TodosContext.Provider>
  );
}

export {
  TodosContext,
  TodosContextProvider,
}
